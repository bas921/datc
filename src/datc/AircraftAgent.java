/* Copyright (C) Joppe Lauriks & Bas van Otterloo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by:
 * Joppe Lauriks <joppe.lauriks@hva.nl>
 * Bas van Otterloo <bas.van.otterloo@hva.nl>
 *
 * Date: December 2014
 */
package datc;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.*;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The aircraft agent contains all the behaviour for the aircraft to automatically land.
 * 
 * @author Joppe Lauriks
 * @author Bas van Otterloo
 */
public class AircraftAgent extends Agent {
    
    private final ServiceDescription sd = new ServiceDescription();
    private final DFAgentDescription dfd = new DFAgentDescription();
    
    private String flightNumber;
    private String tailNumber;
    private String aircraftState;
    
    private int authoritativeAircrafts;
    
    private int authoritativeConfirmations;

    /**
     * Automatically adds one authoritative confirmation and returns it.
     * @return  The authoritative confirmations +1
     */
    public int getAuthoritativeConfirmations() {
        return ++authoritativeConfirmations;
    }

    /**
     * Resets the authoritative confirmations to 0
     */
    public void resetAuthoritativeConfirmations() {
        this.authoritativeConfirmations = 0;
    }

    /**
     * Gets the number of authoritative aircrafts
     * @return The number of currently known authoritative aircrafts
     */
    public int getAuthoritativeAircrafts() {
        return authoritativeAircrafts;
    }

    /**
     * Sets the number of known authoritative aircrafts
     * @param authoritativeAircrafts The new number of authoritative aircrafts
     */
    public void setAuthoritativeAircrafts(int authoritativeAircrafts) {
        this.authoritativeAircrafts = authoritativeAircrafts;
    }

    /**
     * Gets the flight number
     * @return The flight number
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Gets the tail number
     * @return The tail number
     */
    public String getTailNumber() {
        return tailNumber;
    }

    /**
     * Gets the current aircraft state
     * @return The current aircraft state
     */
    public String getAircraftState() {
        return aircraftState;
    }

    /**
     * Sets the flight number
     * @param flightNumber The new flight number
     */
    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    /**
     * Sets a new tail number
     * @param tailNumber The new tail number
     */
    public void setTailNumber(String tailNumber) {
        this.tailNumber = tailNumber;
    }

    /**
     * Sets the aircraft state to a known aircraft state
     * Known aircraft states:
     * - Preparing: Aircraft is on the ground preparing
     * - Flying: The aircraft is in the air flying
     * - Taking off: The aircraft is currently using a runway for take-off
     * - Approaching: The aircraft is approaching/preparing for a landing
     * - Landing: The aircraft is currently using a runway for landing
     * @param state A known aircraft state
     */
    public void setAircraftState(String state) {
        // First set the actual variable
        switch(state){
            case "preparing":
                this.aircraftState = "preparing";
                break;
            case "flying":
                this.aircraftState = "flying";
                break;
            case "taking_off":
                this.aircraftState = "taking_off";
                break;
            case "approaching":
                this.aircraftState = "approaching";
                break;
            case "requesting":
                this.aircraftState = "requesting";
                break;
            case "landing":
                this.aircraftState = "landing";
                break;
            default:
                this.aircraftState = "preparing";
                break;  
        }
        
        // Also set the aircraft state in the Yellow Pages
        sd.addProperties(new Property("aircraft-state", getAircraftState()));
        
        try {
            DFService.modify(this, dfd);
        } catch (FIPAException ex) {
            Logger.getLogger(AircraftAgent.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    protected void setup(){        
        Random rand = new Random(); // For random numbers
        
        Queue approaching; // WIP, currently partially used.
        approaching = new LinkedList(); // WIP: Queue for aircrafts approaching
        setTailNumber("PH" + (rand.nextInt(9000) + 1000)); // Set a random cool-looking PH (the Netherlands) tail number
        setFlightNumber("JB" + (rand.nextInt(9000) + 1000)); // Set a random cool-looking flight number (JB: Joppe Bas)
                
        // Register everything with the Directory Facilitator
        dfd.setName( getAID() );
        sd.setType("aircraft");
        sd.setName(getLocalName());
        sd.addProperties(new Property("tail-number", getTailNumber()));
        sd.addProperties(new Property("flight-number", getFlightNumber()));
       
        dfd.addServices(sd); // Add the service
        
        
        
        try {
            DFService.register(this, dfd); // Register in Yellow Pages (directory)
        } catch (FIPAException ex) {
            Logger.getLogger(AircraftAgent.class.getName()).log(Level.SEVERE, null, ex);
        }

        setAircraftState("flying"); // Let's fly!
        
        /**
         * Here the behaviours will be defined through anonymous functions.
         * 
         * Each performative will have it's own Cyclic behaviour
         */
        addBehaviour(new CyclicBehaviour( this ) {
            
            /**
             * For receiving informative messages
             */
            @Override
            public void action() {
                ACLMessage msg = receive( MessageTemplate.MatchPerformative( ACLMessage.INFORM ) );
                
                /**
                 * Currently receiving the following Informative messages:
                 * 
                 * - HELLO: A way of aircrafts greeting each other
                 * - THANKS: A way of aircrafts thanking each other
                 * - AIRCRAFT_DISCOVERY: For informing other aircrafts about other aircrafts
                 * -- Looks like AIRCRAFT_DISCOVERY:(number_of_aircrafts)
                 */
                if(msg != null){
                    if (PilotAgent.HELLO.equals( msg.getContent() )) {
                        // If you receive a HELLO message, send THANKS back
                        ACLMessage reply = msg.createReply();
                        reply.setPerformative( ACLMessage.INFORM );
                        reply.setContent(PilotAgent.THANKS);
                        send(reply);                        
                    }else if(PilotAgent.THANKS.equals( msg.getContent() )){
                        // A friendly thanks!
                        // Unused, could be used for distributed discovery?
                    }else if(msg.getContent().startsWith(PilotAgent.AIRCRAFT_DISCOVERY + ":")){
                        // If another aircraft informs about AIRCRAFT_DISCOVERY, updated it
                        String[] discovered = msg.getContent().split(":");
                        int aircrafts = Integer.parseInt(discovered[1]);
                        setAuthoritativeAircrafts(aircrafts-1); // All aircrafts excluding current
                    }
                }else{
                    block();
                }
                
            }
            
            
        });
        
        addBehaviour( new CyclicBehaviour( this ) {
            /**
             * For receiving requests
             */

            @Override
            public void action() {
                ACLMessage msg = receive( MessageTemplate.MatchPerformative( ACLMessage.REQUEST ) );
                
                /**
                 * Currently receiving the following requests:
                 * 
                 * - LANDING: An aircraft wants to land
                 */
                if(msg != null){
                    if (PilotAgent.LANDING.equals( msg.getContent() )) {
                        // An aircraft wants to land
                        ACLMessage reply = msg.createReply(); // Let's reply with an answer
                        if(getAircraftState().equals("landing") || getAircraftState().equals("requesting")) { // If we are landing, refuse
                            reply.setPerformative( ACLMessage.REFUSE );
                        } else { // We are not landing, so confirm
                            reply.setPerformative( ACLMessage.CONFIRM );
                        }
                        reply.setContent(PilotAgent.LANDING);
                        approaching.add(msg.getSender());
                        send(reply);                        
                    }
                }else{
                    block();
                }
            }            
        });
        
        
        addBehaviour(new CyclicBehaviour( this ) {
            /**
             * For receiving refusals
             */

            @Override
            public void action() {
                ACLMessage msg = receive( MessageTemplate.MatchPerformative( ACLMessage.REFUSE ) );
                /**
                 * Currently receiving the following refusals:
                 * 
                 * - LANDING: An other aircraft refuses you can land
                 * 
                 */
                if(msg != null){
                    if (PilotAgent.LANDING.equals( msg.getContent() )) {
                        // It' a landing confirmation!
                        // NOTE: getAuthoritatveAircrafts() automatically increments with +1 before returning!
                        setAircraftState("approaching"); // Try to make another landing next time
                    }
                }else{
                    block();
                }
                
            }
            
            
            
            
        });
                
        addBehaviour(new CyclicBehaviour( this ) {
            /**
             * For receiving confirmations
             */

            @Override
            public void action() {
                ACLMessage msg = receive( MessageTemplate.MatchPerformative( ACLMessage.CONFIRM ) );
                /**
                 * Currently receiving the following confirmations:
                 * 
                 * - LANDING: An other aircraft confirms you can land
                 * 
                 */
                if(msg != null){
                    if (PilotAgent.LANDING.equals( msg.getContent() )) {
                        // It' a landing confirmation!
                        // NOTE: getAuthoritatveAircrafts() automatically increments with +1 before returning!
                        if(getAuthoritativeConfirmations() == getAuthoritativeAircrafts()){ // All aircrafts need to agree
                            setAircraftState("landing"); // Let's do this!
                            System.out.println("[" + getLocalName() + "] All authoritative aircrafts confirmed! Landing...");
                            
                            // Behaviour for simulating a landing after ten seconds
                            addBehaviour(new WakerBehaviour( myAgent , 10000 ){
                                protected void onWake(){
                                    // And we have landed!
                                    System.out.println("[" + getLocalName() + "] Landed");
                                    setAircraftState("preparing"); // Set state to preparing for take-off
                                }
                            });
                        }
                    }
                }else{
                    block();
                }
                
            }
            
            
            
            
        });
        
         /**
         * Random Landing Behavior between every 5 seconds
         */
        addBehaviour( new TickerBehaviour(this, 5000){

            @Override
            protected void onTick() {
                switch(getAircraftState()){
                    case "approaching":
                        // Already in approach, previous landing attempt was denied
                        setAircraftState("requesting");
                        resetAuthoritativeConfirmations(); // Reset confirmations
                        broadcastMessage(ACLMessage.REQUEST, PilotAgent.LANDING); // Try for another landing 
                        break;
                    case "flying":
                        int chance = rand.nextInt(101);
                        if(chance > 95){ // We got a ~five percent change to land
                            resetAuthoritativeConfirmations(); // Just to be sure
                            setAircraftState("requesting");
                            System.out.println("[" + getLocalName() + "] Requesting landing");
                            broadcastMessage(ACLMessage.REQUEST, PilotAgent.LANDING);
                        }
                        break;
                }
            }
            
        });
        
        // All behaviours are registered. Going for aircraft discovery
        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription templateSd = new ServiceDescription();
        templateSd.setType("aircraft");
        template.addServices(templateSd);
        
        DFAgentDescription [] agents; // Will contain all the available agents

        SearchConstraints ALL = new SearchConstraints();
        ALL.setMaxResults ( new Long(-1) );
        try {
            agents = DFService.search( this, template, ALL ); // Get all the known aircrafts from directory
            broadcastMessage(ACLMessage.INFORM, PilotAgent.AIRCRAFT_DISCOVERY + ":" + agents.length); // Broadcast them
        } catch (FIPAException ex) {
            Logger.getLogger(AircraftAgent.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        
    }
    
    /**
     * Will broadcast message to all agents
     * 
     * @param performative The ACLMessage performative
     * @param content Message content
     */
    protected void broadcastMessage(int performative, String content) {
        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription templateSd = new ServiceDescription();
        templateSd.setType("aircraft");
        template.addServices(templateSd);
        
        DFAgentDescription [] agents; // Will contain all the available agents
        
        // Create the message with performative and content
        ACLMessage msg = new ACLMessage( performative );
        msg.setContent( content );
        AID myID = getAID();
        
        try {
            SearchConstraints ALL = new SearchConstraints(); // Get all the aircrafts
            ALL.setMaxResults ( new Long(-1) );
            agents = DFService.search( this, template, ALL );
            
            
            for (DFAgentDescription agent : agents) {
                AID agentID = agent.getName();
                if(!agentID.equals( myID )){
                    msg.addReceiver(agentID);
                }
            }

            send(msg);
        }catch(Exception e){
            System.out.println("Error sending message");
        }
    }
}
