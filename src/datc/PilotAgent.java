/* Copyright (C) Joppe Lauriks & Bas van Otterloo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by:
 * Joppe Lauriks <joppe.lauriks@hva.nl>
 * Bas van Otterloo <bas.van.otterloo@hva.nl>
 *
 * Date: December 2014
 */
package datc;

import jade.core.Agent;
import jade.wrapper.AgentController;
import jade.wrapper.PlatformController;

/**
 *
 * The Pilot Agent is for "spawning" aircrafts defining their communication
 * 
 * @author Joppe Lauriks
 * @author Bas van Otterloo
 */
public class PilotAgent extends Agent{
    
    /** 
     * Initialization messages
     */
    public final static String HELLO = "Have a good flight, over.";
    public final static String THANKS = "Thank you, over.";
    
    /**
     * Spectate messages
     */
    public final static String OBSERVE = "OBSERVE";
    public final static String AIRCRAFT_DISCOVERY = "AIRCRAFT_DISCOVERY";
    
    /**
     * State messages
     */
    public final static String PREPARING = "PREPARING";
    public final static String TAKING_OFF = "TAKING_OFF";
    public final static String LANDING = "LANDING";
    public final static String FLYING = "FLYING";
    

    public void setup(){
        System.out.println("Distributed Air Traffic Control pilot initiated.");
        
        int numAircrafts = 50;
        
        // Take off aircrafts
        PlatformController container = getContainerController();
        try{
            for (int i = 0;  i < numAircrafts;  i++) {
                String localName = "aircraft_"+i;
                AgentController aircraft = container.createNewAgent(localName, "datc.AircraftAgent", null);
                aircraft.start();
            }
        }catch(Exception e){
            // TODO: Do something with the exception
        }
        
    }
    
}
