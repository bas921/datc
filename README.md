# Distributed Aircraft Traffic Control
A simple distributed aircraft simulation made in JAVA Agent DEvelopment Framework (JADE).

It currently consists of the following functionality:

* Spawning aircrafts through a pilot agent
* Random tail and flight numbering
* Simple Ping-Pong (Hello/Thanks) handshake
* Broadcast messaging across all Aircraft Agents (powered by the Directory Facilitator)
* Semi-distributed aircraft discovery (powered by the Directory Facilitator)
* Random landings requesting confirmation of all authoritative aircrafts (currently all AircraftAgents)
* Aircraft states (preparing, flying, approaching, landing, takingoff)
* Automatic retries after first landing approach got denied
* ...and more

## Usage
The project is wrapped in a Netbeans Project. The JADE libraries are included in /lib.

Using the following run arguments to launch:

    -gui PilotAgent:datc.PilotAgent